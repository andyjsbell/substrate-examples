[package]
name = 'cf-integration-tests'
version = '0.1.0'
authors = ['Chainflip Team <https://github.com/chainflip-io>']
edition = '2018'
homepage = 'https://chainflip.io'
license = '<TODO>'
publish = false
repository = 'https://github.com/chainflip-io/chainflip-backend'

[package.metadata.docs.rs]
targets = ['x86_64-unknown-linux-gnu']

[dependencies]
libsecp256k1 = { default-features = false, version = '0.7', features = ['static-context'] }
rand = {version='0.8.4'}

# Chainflip local dependencies
state-chain-runtime = { path = "../runtime", default-features = false }
cf-traits = { path = "../traits", default-features = false }
cf-chains = { path = "../chains", default-features = false }
pallet-cf-validator = { path = "../pallets/cf-validator", default-features = false }
pallet-cf-staking = { path = "../pallets/cf-staking", default-features = false }
pallet-cf-witnesser = { path = "../pallets/cf-witnesser", default-features = false}
pallet-cf-flip = { path = "../pallets/cf-flip", default-features = false }
pallet-cf-auction = { path = "../pallets/cf-auction", default-features = false }
pallet-cf-emissions = { path = "../pallets/cf-emissions", default-features = false }
pallet-cf-witnesser-api = { path = "../pallets/cf-witnesser-api", default-features = false }
pallet-cf-governance = { path = "../pallets/cf-governance", default-features = false }
pallet-cf-reputation = { path = "../pallets/cf-reputation", default-features = false }
pallet-cf-vaults = { path = "../pallets/cf-vaults", default-features = false }
pallet-cf-online = { path = "../pallets/cf-online", default-features = false }
pallet-cf-threshold-signature = { path = "../pallets/cf-threshold-signature", default-features = false }
pallet-cf-broadcast = { path = "../pallets/cf-broadcast", default-features = false }
pallet-cf-environment = { path = "../pallets/cf-environment", default-features = false }


# Additional FRAME pallets
[dependencies.pallet-authorship]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.pallet-session]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'
features = ['historical']

# Substrate dependencies
[dependencies.codec]
default-features = false
features = ['derive']
package = 'parity-scale-codec'
version = '2.3.1'

[dependencies.frame-benchmarking]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
optional = true
tag = 'merge-config-option'

[dependencies.frame-executive]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.frame-support]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.frame-system]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.frame-system-benchmarking]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
optional = true
tag = 'merge-config-option'

[dependencies.frame-system-rpc-runtime-api]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dev-dependencies.hex-literal]
version = '0.3.1'

[dependencies.pallet-aura]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.pallet-grandpa]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.pallet-randomness-collective-flip]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.pallet-timestamp]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.pallet-transaction-payment]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.scale-info]
default-features = false
features = ['derive']
version = '1.0'

[dependencies.sp-block-builder]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.sp-consensus-aura]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'
version = '0.10.0-dev'

[dependencies.sp-core]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.sp-inherents]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.sp-offchain]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.sp-runtime]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.sp-session]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.sp-std]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.sp-transaction-pool]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dependencies.sp-version]
default-features = false
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[dev-dependencies]
assert_matches = { version = '1.5.0' }

[dev-dependencies.sp-finality-grandpa]
git = 'https://github.com/chainflip-io/substrate.git'
tag = 'merge-config-option'

[features]
default = ['std']
std = [
  'state-chain-runtime/std',
  'cf-traits/std',
  'cf-chains/std',
  'codec/std',
  'scale-info/std',
  'frame-executive/std',
  'frame-support/std',
  'frame-system/std',
  'frame-system-rpc-runtime-api/std',
  'pallet-aura/std',
  'pallet-grandpa/std',
  'pallet-randomness-collective-flip/std',
  'pallet-session/std',
  'pallet-timestamp/std',
  'pallet-transaction-payment/std',
  'sp-block-builder/std',
  'sp-consensus-aura/std',
  'sp-core/std',
  'sp-inherents/std',
  'sp-offchain/std',
  'sp-runtime/std',
  'sp-session/std',
  'sp-std/std',
  'sp-transaction-pool/std',
  'sp-version/std',
  'pallet-cf-validator/std',
  'pallet-cf-staking/std',
  'pallet-cf-witnesser/std',
  'pallet-cf-flip/std',
  'pallet-cf-auction/std',
  'pallet-cf-emissions/std',
  'pallet-cf-reputation/std',
  'pallet-cf-witnesser-api/std',
  'pallet-cf-governance/std',
  'pallet-cf-vaults/std',
  'pallet-cf-broadcast/std',
  'pallet-cf-threshold-signature/std',
  'pallet-cf-environment/std',
  'pallet-cf-online/std',
]
