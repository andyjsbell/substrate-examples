# Chainflip Validator Pallet

A module to manage the validator set for the Chainflip State Chain

## Overview

The module contains functionality to manage the validator set used to ensure the Chainflip
State Chain network.  It extends on the functionality offered by the `session` pallet provided by
Parity. There are two types of sessions; an Epoch session in which we have a constant set of validators
and an Auction session in which we continue with our current validator set and request a set of
candidates for validation.  Once validated and confirmed they become our new set of validators for the
subsequent Epoch.

## Terminology

- Validator: A node that has staked an amount of `FLIP` ERC20 token.
- Validator ID: Equivalent to an Account ID
- Epoch: A period in blocks in which a constant set of validators ensure the network.
- Auction: A non defined period of blocks in which we continue with the existing validators
  and assess the new candidate set of their validity as validators.  This period is closed when a 
  successful vault rotation has occurred.
- Session: A session as defined by the `session` pallet. We have two sessions; Epoch which has
  a fixed number of blocks set with `set_blocks_for_epoch` and an Auction session which is of an
  undetermined number of blocks.
- Emergency Rotation: An emergency rotation can be requested which initiates a new auction and on success of this 
  auction a new validating set will secure the network.
- Claim Period: A percentage of the epoch where by a validator can claim.
### Dispatchable Functions

- `set_blocks_for_epoch` - Set the number of blocks an Epoch should run for.
- `force_rotation` - Force a rotation of validators to start on the next block.
- `update_period_for_claims` - Update the period for claims.
- `register_peer_id` - Register the peer id to their validator id.
- `cfe_version` - Register the CFE version for a validato.r